const express = require("express");
const passport = require("passport");
const User = require("../models/user");
const Workspace = require("../models/workspace");
const cloudinary = require("cloudinary");

const router = express.Router();

router.get("/user", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      res.status(200).send({
        auth: true,
        firstname: user.firstname,
        lastname: user.lastname,
        username: user.username,
        registered: user.registered,
        telegram: user.telegram,
        avatar: user.avatar,
        user: user.admin
      });
    }
  })(req, res, next);
});

router.get("/users", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("members")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate("members")
        .exec();
      if (req.query.workspace_name && !req.query.adding) {
        if (
          allWorkspaces.find(x => x.name === reqWorkspace.name) ||
          user.admin
        ) {
          res.status(200).send({
            auth: true,
            users: reqWorkspace.members
          });
        } else {
          res.status(403).send({
            failed: true,
            message: "Access denied"
          });
        }
      } else {
        const page = req.query.page ? parseInt(req.query.page) : 0;
        let users_to_send = new Array();
        const users = await User.find();
        let sorted_users = users;
        if (req.query.username)
          sorted_users = users.filter(
            x =>
              !x.username.toLowerCase().search(req.query.username.toLowerCase())
          );
        if (req.query.adding) {
          let buff = [];
          for (let i = 0; i < sorted_users.length; i++) {
            let dublicated = false;
            for (
              let j = 0;
              j < reqWorkspace.members.length && !dublicated;
              j++
            ) {
              if (
                String(reqWorkspace.members[j]._id) ===
                String(sorted_users[i]._id)
              ) {
                dublicated = true;
              }
            }
            if (!dublicated) {
              buff.push(sorted_users[i]);
            }
          }
          sorted_users = buff;
        }
        for (
          let i = page * 5;
          i < page * 5 + 5 && i < sorted_users.length;
          i++
        ) {
          users_to_send.push(sorted_users[i]);
        }
        res.status(200).send({
          auth: true,
          users: users_to_send,
          pages:
            sorted_users.length / 5 > (sorted_users.length / 5).toFixed()
              ? Number((sorted_users.length / 5).toFixed()) + 1
              : Number((sorted_users.length / 5).toFixed()),
          admin: user.admin
        });
      }
    }
  })(req, res, next);
});

router.put("/user", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      if (req.body.username === user.username) {
        console.log("user");
        let imageUrl = null;
        if (req.files.image) {
          cloudinary.v2.uploader
            .upload_stream(
              {
                resource_type: "raw"
              },
              async (err, result) => {
                if (err) {
                  res.status(500).send({ failed: true });
                  return;
                }
                imageUrl = result.url;
                const updatedUser = await User.findOneAndUpdate(
                  { username: user.username },
                  {
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    telegram: req.body.telegram,
                    avatar: imageUrl
                  }
                );
                res.status(200).send({
                  auth: true,
                  user: updatedUser
                });
              }
            )
            .end(req.files.image.data);
        } else {
          const updatedUser = await User.findOneAndUpdate(
            { username: user.username },
            {
              firstname: req.body.firstname,
              lastname: req.body.lastname,
              telegram: req.body.telegram
            }
          );
          res.status(200).send({
            auth: true,
            user: updatedUser
          });
        }
      } else if (user.admin) {
        console.log("admin");
        const updatedUser = await User.findOneAndUpdate(
          { username: req.body.username },
          {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            admin: req.body.admin
          }
        );
        res.status(200).send({
          auth: true,
          user: updatedUser
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.delete("/user/:username", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      if (user.admin) {
        await User.findOneAndDelete({ username: req.params.username });
        res.status(200).send({
          auth: true
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

module.exports = router;
