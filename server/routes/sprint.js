const express = require("express");
const passport = require("passport");
const Sprint = require("../models/sprint");
const Workspace = require("../models/workspace");
const WSUsers = require("../config/websocket-users");

const router = express.Router();

router.get("/sprints", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("sprints")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate("sprints")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        res.status(200).send({
          sprints: reqWorkspace.sprints
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.get("/current_sprint", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate({
          path: "currentSprint",
          populate: [
            { path: "pending_stories", populate: { path: "contributor" } },
            { path: "working_stories", populate: { path: "contributor" } },
            { path: "finished_stories", populate: { path: "contributor" } }
          ]
        })
        .populate("members")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate({
          path: "currentSprint",
          populate: [
            { path: "pending_stories", populate: { path: "contributor" } },
            { path: "working_stories", populate: { path: "contributor" } },
            { path: "finished_stories", populate: { path: "contributor" } }
          ]
        })
        .populate("members")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        reqWorkspace.members.forEach(element => {
          const WSUser = WSUsers.find(element.username);
          if (WSUser && WSUser.username !== user.username) {
            const response = Object.assign(
              {},
              { sprint: reqWorkspace.currentSprint },
              { method: "update-sprint" }
            );
            WSUser.websocket.send(JSON.stringify(response));
          }
        });
        res.status(200).send({
          sprint: reqWorkspace.currentSprint
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.get("/sprint/:id", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate({
          path: "sprints",
          populate: [
            { path: "pending_stories", populate: { path: "contributor" } },
            { path: "working_stories", populate: { path: "contributor" } },
            { path: "finished_stories", populate: { path: "contributor" } }
          ]
        })
        .populate("members")
        .populate("admin")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate({
          path: "sprints",
          populate: [
            { path: "pending_stories", populate: { path: "contributor" } },
            { path: "working_stories", populate: { path: "contributor" } },
            { path: "finished_stories", populate: { path: "contributor" } }
          ]
        })
        .populate("members")
        .populate("admin")
        .exec();

      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        let sprint;
        try {
          sprint = await Sprint.findOne({ _id: req.params.id })
            .populate({
              path: "pending_stories",
              populate: { path: "contributor" }
            })
            .populate({
              path: "working_stories",
              populate: { path: "contributor" }
            })
            .populate({
              path: "finished_stories",
              populate: { path: "contributor" }
            })
            .exec();
        } catch (err) {
          res.status(404).send({
            failed: true,
            admin:
              String(reqWorkspace.admin._id) === String(user._id) || user.admin,
            message: "Sprint not found"
          });
        }
        if (
          reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
        ) {
          console.log(
            String(reqWorkspace.admin._id) === String(user._id) || user.admin,
            reqWorkspace.admin._id,
            user._id
          );
          res.status(200).send({
            sprint,
            admin:
              String(reqWorkspace.admin._id) === String(user._id) || user.admin
          });
        } else {
          res.status(404).send({
            failed: true,
            admin:
              String(reqWorkspace.admin._id) === String(user._id) || user.admin,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.post("/sprint", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("currentSprint")
        .populate("sprints")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.body.workspace_name
      })
        .populate("currentSprint")
        .populate("sprints")
        .exec();

      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        let data = {
          pending_stories: req.body.pending_stories,
          working_stories: req.body.working_stories,
          finished_stories: req.body.finished_stories
        };
        if (req.body.duration) data.duration = parseInt(req.body.duration);
        const sprint = await Sprint.create(data);
        reqWorkspace.currentSprint = sprint;
        reqWorkspace.sprints.push(sprint);
        await reqWorkspace.save();
        res.status(200).send({
          sprint
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.delete("/sprint/:id", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("sprints")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate("sprints")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        const sprint = await Sprint.findOne({ _id: req.params.id });
        if (
          reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
        ) {
          sprint.delete().then(() => {
            res.status(200).send({
              workspace: reqWorkspace
            });
          });
        } else {
          res.status(404).send({
            failed: true,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.put("/sprint", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("members")
        .populate("sprints")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.body.workspace_name
      })
        .populate("members")
        .populate("sprints")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        let sprint = await Sprint.findOne({ _id: req.body.sprint_id });
        if (
          reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
        ) {
          if (req.body.duration) {
            sprint.duration = parseInt(req.body.duration);
            sprint.deadline = new Date().setDate(
              sprint.created.getDate() + sprint.duration
            );
          }
          if (req.body.pending_stories) {
            sprint.pending_stories = req.body.pending_stories;
          }
          if (req.body.working_stories) {
            sprint.working_stories = req.body.working_stories;
          }
          if (req.body.finished_stories) {
            sprint.finished_stories = req.body.finished_stories;
          }
          if (req.body.chart) {
            sprint.chart = req.body.chart;
          }
          await sprint.save();
          await Sprint.populate(sprint, [
            {
              path: "pending_stories",
              populate: { path: "contributor" }
            },
            {
              path: "working_stories",
              populate: { path: "contributor" }
            },
            {
              path: "finished_stories",
              populate: { path: "contributor" }
            },
            { path: "contributor" }
          ]);
          reqWorkspace.members.forEach(element => {
            const WSUser = WSUsers.find(element.username);
            console.log(WSUser);
            if (WSUser && WSUser.username !== user.username) {
              console.log("Sending");
              const response = Object.assign(
                {},
                { sprint: sprint },
                { method: "update-sprint" }
              );
              WSUser.websocket.send(JSON.stringify(response));
            }
          });
          res.status(202).send({
            sprint: sprint
          });
        } else {
          res.status(404).send({
            failed: true,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

module.exports = router;
