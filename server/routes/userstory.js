const express = require("express");
const passport = require("passport");
const Userstory = require("../models/userstory");
const Sprint = require("../models/sprint");
const User = require("../models/user");
const Workspace = require("../models/workspace");
const WSUsers = require("../config/websocket-users");
const sendMessage = require("../config/telegram");

const router = express.Router();

router.post("/userstory", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("currentSprint")
        .populate("sprints")
        .populate("members")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.body.workspace_name
      })
        .populate("currentSprint")
        .populate("sprints")
        .populate("members")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        console.log("Hello");
        const sprint = await Sprint.findOne({ _id: req.body.sprint_id });
        if (sprint) {
          if (
            reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
          ) {
            console.log("Hello2");
            const userstory = await Userstory.create({
              title: req.body.title,
              description: req.body.description,
              contributor: req.body.contributor,
              priority: req.body.priority
            });
            await Userstory.populate(userstory, { path: "contributor" });
            sprint.pending_stories.push(userstory);
            await sprint.save();
            reqWorkspace.members.forEach(element => {
              const WSUser = WSUsers.find(element.username);
              if (WSUser && WSUser.username !== user.username) {
                const response = Object.assign(
                  {},
                  { story: story },
                  { method: "new-story" }
                );
                WSUser.websocket.send(JSON.stringify(response));
              }
            });
            if (userstory.contributor.telegram_id) {
              sendMessage({
                chat_id: userstory.contributor.telegram_id,
                text: `You have new story in ${req.body.workspace_name}\n\t${
                  req.body.title
                }\n\tPriority: ${req.body.priority}`
              }).catch(err => {
                console.log(err.message);
              });
            }
            res.status(200).send({
              story: userstory
            });
          } else {
            res.status(404).send({
              failed: true,
              message: "Sprint not found"
            });
          }
        } else {
          res.status(404).send({
            failed: true,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.put("/userstory", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("currentSprint")
        .populate("sprints")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.body.workspace_name
      })
        .populate("currentSprint")
        .populate("sprints")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        const sprint = await Sprint.findOne({ _id: req.body.sprint_id })
          .populate("pending_stories")
          .populate("working_stories")
          .populate("finished_stories")
          .exec();
        if (sprint) {
          if (
            reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
          ) {
            const userstory = await Userstory.findById({
              _id: req.body.userstory_id
            });
            if (userstory) {
              if (
                sprint.pending_stories.filter(
                  x => String(x._id) === String(req.body.userstory_id)
                ).length ||
                sprint.working_stories.filter(
                  x => String(x._id) === String(req.body.userstory_id)
                ).length ||
                sprint.finished_stories.filter(
                  x => String(x._id) === String(req.body.userstory_id)
                ).length
              ) {
                userstory.title = req.body.title;
                userstory.description = req.body.description;
                userstory.contributor = req.body.contributor;
                userstory.priority = req.body.priority;
                await userstory.save();
                await Userstory.populate(userstory, { path: "contributor" });
                const contributor = await User.findById(userstory.contributor);
                if (contributor.telegram_id) {
                  sendMessage({
                    chat_id: contributor.telegram_id,
                    text: `Your story in ${
                      req.body.workspace_name
                    } was updated\n\t${userstory.title}`
                  }).catch(err => {
                    console.log(err.message);
                  });
                }
                res.status(200).send({
                  auth: true,
                  story: userstory
                });
              } else {
                res.status(403).send({
                  failed: true,
                  message: "Access denied"
                });
              }
            } else {
              res.status(404).send({
                failed: true,
                message: "Story not found"
              });
            }
          } else {
            res.status(404).send({
              failed: true,
              message: "Sprint not found"
            });
          }
        } else {
          res.status(404).send({
            failed: true,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.delete("/userstory/:id", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const allWorkspaces = await Workspace.find({ members: user._id })
        .populate("currentSprint")
        .populate("sprints")
        .populate("members")
        .exec();
      const reqWorkspace = await Workspace.findOne({
        name: req.query.workspace_name
      })
        .populate("currentSprint")
        .populate("sprints")
        .populate("members")
        .exec();
      if (allWorkspaces.find(x => x.name === reqWorkspace.name) || user.admin) {
        const sprint = await Sprint.findOne({ _id: req.query.sprint_id })
          .populate("pending_stories")
          .populate("working_stories")
          .populate("finished_stories")
          .exec();
        if (sprint) {
          if (
            reqWorkspace.sprints.find(x => String(x._id) === String(sprint._id))
          ) {
            const userstory = await Userstory.findById({ _id: req.params.id })
              .populate("contributor")
              .exec();
            if (userstory) {
              if (
                sprint.pending_stories.filter(
                  x => String(x._id) === String(req.params.id)
                ).length ||
                sprint.working_stories.filter(
                  x => String(x._id) === String(req.params.id)
                ).length ||
                sprint.finished_stories.filter(
                  x => String(x._id) === String(req.params.id)
                ).length
              ) {
                await userstory.delete();
                reqWorkspace.members.forEach(element => {
                  const WSUser = WSUsers.find(element.username);
                  if (WSUser && WSUser.username !== user.username) {
                    const response = Object.assign(
                      {},
                      { story: story },
                      { method: "delete-story" }
                    );
                    WSUser.websocket.send(JSON.stringify(response));
                  }
                });
                if (userstory.contributor.telegram_id) {
                  sendMessage({
                    chat_id: userstory.contributor.telegram_id,
                    text: `Your story in ${
                      req.query.workspace_name
                    } was deleted\n\t${userstory.title}`
                  }).catch(err => {
                    console.log(err.message);
                  });
                }
                res.status(200).send({
                  auth: true
                });
              } else {
                res.status(403).send({
                  failed: true,
                  message: "Access denied"
                });
              }
            } else {
              res.status(404).send({
                failed: true,
                message: "Story not found"
              });
            }
          } else {
            res.status(404).send({
              failed: true,
              message: "Sprint not found"
            });
          }
        } else {
          res.status(404).send({
            failed: true,
            message: "Sprint not found"
          });
        }
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

module.exports = router;
