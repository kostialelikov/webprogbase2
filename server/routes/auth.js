const express = require("express");
const passport = require("passport");
const User = require("../models/user");
const config = require("../config/index");
const jwt = require("jsonwebtoken");

const router = express.Router();

router.post("/signup", function(req, res, next) {
  passport.authenticate("register", (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      console.log(user);
      req.logIn(user, err => {
        const data = {
          username: user.username,
          firstname: req.body.firstname,
          lastname: req.body.lastname
        };
        User.findOne({ username: user.username }).then(user => {
          user
            .update({ firstname: data.firstname, lastname: data.lastname })
            .then(() => {
              const token = jwt.sign({ user: user }, config.SECRET);
              res.status(200).send({
                auth: true,
                token: token,
                user: user,
                message: "Successful registration"
              });
            });
        });
      });
    }
  })(req, res, next);
});

router.post("/login", (req, res, next) => {
  passport.authenticate("login", (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      req.logIn(user, err => {
        User.findOne({ username: user.username }).then(user => {
          const token = jwt.sign({ user: user }, config.SECRET);
          res.status(200).send({
            auth: true,
            token: token,
            user: user,
            message: "Successful login"
          });
        });
      });
    }
  })(req, res, next);
});

router.post("/checkusername", (req, res) => {
  User.findOne({ username: req.body.username }).then(user => {
    if (user) {
      res.send({ free: false });
    } else {
      res.send({ free: true });
    }
  });
});

module.exports = router;
