const express = require("express");
const passport = require("passport");
const mongoose = require("mongoose");
const Workspace = require("../models/workspace");
const sendMessage = require("../config/telegram");

const router = express.Router();

Array.prototype.diff = function(a) {
  return this.filter(function(i) {
    return a.indexOf(i) === -1;
  });
};

router.get("/isAdmin/:name", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      Workspace.findOne({ name: req.params.name })
        .populate("admin")
        .populate("members")
        .populate("currentSprint")
        .populate("sprints")
        .exec()
        .then(async workspace => {
          if (workspace) {
            if (
              !workspace.members.filter(x => x.username === user.username) &&
              !user.admin
            ) {
              res.status(403).send({
                failed: true,
                message: "Access denied"
              });
            } else {
              res.status(200).send({
                auth: true,
                admin:
                  String(workspace.admin._id) === String(user._id) || user.admin
              });
            }
          } else {
            res.status(404).send({
              failed: true
            });
          }
        });
    }
  })(req, res, next);
});

router.get("/workspaces", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      let workspaces;
      if (user.admin) {
        workspaces = await Workspace.find()
          .populate("admin")
          .populate("members")
          .populate("currentSprint")
          .populate("sprints")
          .exec();
      } else {
        workspaces = await Workspace.find({
          members: mongoose.Types.ObjectId(user._id)
        })
          .populate("admin")
          .populate("members")
          .populate("currentSprint")
          .populate("sprints")
          .exec();
      }
      let empty = true;
      if (workspaces.length !== 0) {
        empty = false;
      }
      const page = req.query.page ? parseInt(req.query.page) : 0;
      let workspacesToSend = new Array();
      for (let i = page * 5; i < page * 5 + 5 && i < workspaces.length; i++) {
        workspacesToSend.push(workspaces[i]);
      }
      if (req.query.name) {
        workspacesToSend = workspacesToSend.filter(
          x => !x.name.toLowerCase().search(req.query.name.toLowerCase())
        );
      }
      res.status(200).send({
        auth: true,
        workspaces: workspacesToSend,
        pages:
          workspaces.length / 5 > (workspaces.length / 5).toFixed()
            ? Number((workspaces.length / 5).toFixed()) + 1
            : Number((workspaces.length / 5).toFixed()),
        empty
      });
    }
  })(req, res, next);
});

router.get("/workspace/:name", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      Workspace.findOne({ name: req.params.name })
        .populate("admin")
        .populate("members")
        .populate("currentSprint")
        .populate("sprints")
        .exec()
        .then(async workspace => {
          if (workspace) {
            if (
              !workspace.members.filter(x => x.username === user.username) &&
              !user.admin
            ) {
              res.status(403).send({
                failed: true,
                message: "Access denied"
              });
            } else {
              if (workspace.currentSprint) {
                if (Date(workspace.currentSprint.deadline) <= Date.now()) {
                  workspace.currentSprint = "";
                  await workspace.save();
                }
              }
              res.status(200).send({
                auth: true,
                workspace,
                admin:
                  String(workspace.admin._id) === String(user._id) || user.admin
              });
            }
          } else {
            res.status(404).send({
              failed: true
            });
          }
        });
    }
  })(req, res, next);
});

router.post("/workspace", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      Workspace.findOne({ name: req.body.name }).then(workspace => {
        if (workspace) {
          res.status(409).send({
            failed: true,
            message:
              "Workspace with name this all ready exist. Please choose another name."
          });
        } else {
          Workspace.create({
            name: req.body.name,
            description: req.body.description,
            admin: user,
            members: [user]
          }).then(data => {
            res.status(201).send({
              workspace: data
            });
          });
        }
      });
    }
  })(req, res, next);
});

router.delete("/workspace/:name", async (req, res, next) => {
  passport.authenticate("jwt", { session: false }, async (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      const workspace = await Workspace.findOne({ name: req.params.name })
        .populate("admin")
        .populate("members")
        .exec();
      if (
        user.admin ||
        (workspace.admin && workspace.admin.username === user.username)
      ) {
        await Workspace.findOneAndDelete({ name: req.params.name });
        res.status(200).send({
          auth: true
        });
      } else if (
        workspace.members.find(x => String(x._id) === String(user._id))
      ) {
        const member = workspace.members.find(
          x => String(x._id) === String(user._id)
        );
        workspace.members.splice(workspace.members.indexOf(member), 1);
        await workspace.save();
        res.status(200).send({
          auth: true
        });
      } else {
        res.status(403).send({
          failed: true,
          message: "Access denied"
        });
      }
    }
  })(req, res, next);
});

router.put("/workspace", (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      console.log(err);
    }
    if (info !== undefined) {
      console.log(info.message);
      res.send({
        failed: true,
        message: info.message
      });
    } else {
      Workspace.findOne({ name: req.body.name })
        .populate("admin")
        .populate("members")
        .exec()
        .then(workspace => {
          if (workspace.admin.username !== user.username && !user.admin) {
            res.status(403).send({
              failed: true,
              message: "Access denied"
            });
          } else {
            if (req.body.name) workspace.name = req.body.name;
            if (req.body.admin) workspace.admin = req.body.admin;
            if (req.body.members) {
              let workspaceIds = [];
              let reqIds = [];
              workspace.members.forEach(element => {
                workspaceIds.push(String(element._id));
              });
              req.body.members.forEach(element => {
                reqIds.push(String(element._id));
              });
              let tmp;
              if (workspace.members.length > req.body.members.length) {
                tmp = workspaceIds.diff(reqIds);
                tmp.forEach(element => {
                  const newMember = workspace.members.find(x => {
                    return String(x._id) === element;
                  });
                  if (newMember && newMember.telegram_id > 0) {
                    sendMessage({
                      chat_id: newMember.telegram_id,
                      text: `You have been kicked from ${req.body.name}`
                    });
                  }
                });
              } else {
                tmp = reqIds.diff(workspaceIds);
                tmp.forEach(element => {
                  const newMember = req.body.members.find(x => {
                    return String(x._id) === element;
                  });
                  if (newMember && newMember.telegram_id > 0) {
                    sendMessage({
                      chat_id: newMember.telegram_id,
                      text: `You have been added to ${req.body.name}`
                    });
                  }
                });
              }
              workspace.members = req.body.members;
            }
            if (req.body.currentSprint)
              workspace.currentSprint = req.body.currentSprint;
            if (req.body.sprints) workspace.sprints = req.body.sprints;

            workspace.save().then(data => {
              res.status(202).send({
                workspace: data
              });
            });
          }
        });
    }
  })(req, res, next);
});

module.exports = router;
