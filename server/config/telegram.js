const mongoose = require("mongoose");
const Telegram = require("telegram-bot-api");
const User = require("../models/user");
const Workspace = require("../models/workspace");
const Userstory = require("../models/userstory");
const Sprint = require("../models/sprint");
const config = require("../config/index");

const telegram = new Telegram({
  token: config.TELEGRAM_BOT_TOKEN,
  updates: {
    enabled: true
  }
});

const processMessage = async function(message) {
  const user = await User.findOne({ telegram: message.from.username });
  if (user) {
    if (message.text === "/start") {
      await User.findByIdAndUpdate(
        { _id: user._id },
        { telegram_id: message.chat.id }
      );
    } else if (message.text === "/workspaces") {
      const workspaces = await Workspace.find({ members: user._id });
      let text = "Your workspaces:\n";
      workspaces.forEach(element => {
        text += element.name + "\n";
      });
      telegram.sendMessage({
        chat_id: message.chat.id,
        text
      });
    } else if (message.text.startsWith("/userstories")) {
      const workspaceName = message.text.split(" ")[1];
      const workspace = await Workspace.findOne({
        name: workspaceName,
        members: user._id
      })
        .populate({
          path: "currentSprint",
          populate: [
            { path: "pending_stories", populate: { path: "contributor" } },
            { path: "working_stories", populate: { path: "contributor" } },
            { path: "finished_stories", populate: { path: "contributor" } }
          ]
        })
        .exec();
      if (workspace) {
        let text = "Your stories:\n";
        text += "\tPending:\n";
        workspace.currentSprint.pending_stories.forEach(element => {
          if (element.contributor.username === user.username) {
            text += "\t" + element.title + "\n";
          }
        });
        text += "\n\tWorking:\n";
        workspace.currentSprint.working_stories.forEach(element => {
          if (element.contributor.username === user.username) {
            text += "\t" + element.title + "\n";
          }
        });
        text += "\n\tClosed:\n";
        workspace.currentSprint.finished_stories.forEach(element => {
          if (element.contributor.username === user.username) {
            text += "\t" + element.title + "\n";
          }
        });
        telegram.sendMessage({
          chat_id: message.chat.id,
          text,
          parse_mode: "Markdown"
        });
      } else {
        telegram.sendMessage({
          chat_id: message.chat.id,
          text: "Can't find workspace!\nUsage: `/userstories [Workspace name]`",
          parse_mode: "Markdown"
        });
      }
    }
  } else {
    telegram.sendMessage({
      chat_id: message.chat.id,
      text: "Check are you registered on streamline-scrum.herokuapp.com"
    });
  }
};

telegram.on("message", message => {
  console.log(message);
  processMessage(message);
});

module.exports = telegram.sendMessage;
