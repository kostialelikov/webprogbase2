let users = [];

const WSUsers = {
  append: function(user) {
    console.log("append");
    users.push(user);
  },
  remove: function(websocket) {
    console.log("remove");
    users = users.filter(x => x.websocket !== websocket);
  },
  find: function(username) {
    return users.find(x => x.username === username);
  },
  log: function() {
    console.log(users);
  }
};

module.exports = WSUsers;
