const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const config = require("./index");
const User = require("../models/user");
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

passport.use(
  "register",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      session: false
    },
    (username, password, done) => {
      try {
        User.findOne({ username: username }).then(user => {
          if (user) {
            return done(null, false, {
              message: "Sign up error: Username all ready taken"
            });
          } else {
            User.create({ username, password }).then(user => {
              return done(null, user);
            });
          }
        });
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  "login",
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      session: false
    },
    (username, password, done) => {
      try {
        User.findOne({
          username: username
        }).then(async user => {
          if (!user) {
            return done(null, false, { message: "User not found" });
          } else {
            if (await user.isValidPassword(password)) {
              return done(null, user);
            } else {
              return done(null, false, { message: "Invalid password" });
            }
          }
        });
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  "jwt",
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.SECRET
    },
    (jwt_payload, done) => {
      try {
        User.findOne({ username: jwt_payload.user.username }).then(user => {
          if (user) {
            done(null, user);
          } else {
            done(null, false);
          }
        });
      } catch (error) {
        done(error);
      }
    }
  )
);
