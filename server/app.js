const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const busboy = require("busboy-body-parser");
const passport = require("passport");
const morgan = require("morgan");
const cors = require("cors");
const config = require("./config/index");
const serveStatic = require("serve-static");
const jwt = require("jsonwebtoken");
const path = require("path");
const ws = require("ws");
const http = require("http");
const WSUsers = require("./config/websocket-users");
const cloudinary = require("cloudinary");

const app = express();

cloudinary.config(config.cloudinary);

require("./config/passport");

app.use(serveStatic(__dirname));
app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(busboy());
app.use(passport.initialize());

mongoose
  .connect(
    config.DATABASE_URL,
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log("Database connected");
  })
  .catch(error => {
    console.log("Database connection error: " + error.message);
  });

app.use("/", require("./routes/auth"));
app.use("/", require("./routes/user"));
app.use("/", require("./routes/workspace"));
app.use("/", require("./routes/sprint"));
app.use("/", require("./routes/userstory"));

app.get("/api", (req, res) => {
  res.sendFile(path.join(__dirname + "/api.html"));
});

app.use((req, res) => {
  res.status(500).json({ message: "Server error" });
});

const httpServer = http.createServer(app);

httpServer.listen(process.env.PORT || config.PORT, () => {
  console.log("Server started on port: " + process.env.PORT || config.PORT);
});

const wsServer = new ws.Server({ server: httpServer });

wsServer.on("connection", websock => {
  websock.on("message", request => {
    request = JSON.parse(request);
    switch (request.method) {
      case "add-user": {
        if (request.jwt) {
          const WSUser = Object.assign({}, jwt.decode(request.jwt).user, {
            websocket: websock
          });
          if (!WSUsers.find(WSUser.username)) WSUsers.append(WSUser);
        }
        break;
      }
      case "remove-user": {
        WSUsers.remove(websock);
      }
    }
  });
  websock.on("close", () => {
    WSUsers.remove(websock);
  });
});

setInterval(() => {
  wsServer.clients.forEach(client => {
    client.ping();
  });
}, 1000);
