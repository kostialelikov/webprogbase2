const mongoose = require("mongoose");

const WorkspaceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    default: ""
  },
  admin: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "users"
  },
  members: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "users"
    }
  ],
  currentSprint: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "sprints"
  },
  sprints: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "sprints"
    }
  ]
});

const Workspace = mongoose.model("workspaces", WorkspaceSchema);

module.exports = Workspace;
