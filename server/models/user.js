const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const UserSchema = new mongoose.Schema({
  username: {
    type: mongoose.SchemaTypes.String,
    required: true,
    unique: true
  },
  password: {
    type: mongoose.SchemaTypes.String,
    required: true
  },
  firstname: {
    type: mongoose.SchemaTypes.String
  },
  lastname: {
    type: mongoose.SchemaTypes.String
  },
  admin: {
    type: mongoose.SchemaTypes.Boolean,
    default: false
  },
  registered: {
    type: mongoose.SchemaTypes.Date,
    default: Date.now()
  },
  telegram: {
    type: mongoose.SchemaTypes.String,
    default: ""
  },
  telegram_id: {
    type: mongoose.SchemaTypes.String,
    default: 0
  },
  avatar: {
    type: mongoose.SchemaTypes.String,
    default:
      "https://res.cloudinary.com/streamline-scrum/image/upload/v1541324100/no_image.png"
  }
});

UserSchema.pre("save", async function(next) {
  const user = this;
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;
  next();
});

UserSchema.methods.isValidPassword = async function(password) {
  const user = this;
  const compare = await bcrypt.compare(password, user.password);
  console.log(compare + password + user.password);
  return compare;
};

const UserModel = mongoose.model("users", UserSchema);

module.exports = UserModel;
