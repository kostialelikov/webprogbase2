const mongoose = require("mongoose");

const UserstoryScheme = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: String,
  contributor: {
    type: mongoose.mongo.ObjectId,
    ref: "users"
  },
  priority: {
    type: String,
    enum: ["Not important", "Low", "Middle", "High", "Critical"],
    default: "Not important"
  }
});

const Userstory = mongoose.model("userstories", UserstoryScheme);

module.exports = Userstory;
