const mongoose = require("mongoose");

Date.prototype.addDays = function(days) {
  let date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

const SprintSchema = new mongoose.Schema({
  created: {
    type: mongoose.SchemaTypes.Date,
    default: new Date()
  },
  duration: {
    type: mongoose.SchemaTypes.Number,
    default: 14
  },
  deadline: {
    type: mongoose.SchemaTypes.Date,
    default: function() {
      return new Date().addDays(this.duration);
    }
  },
  pending_stories: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "userstories"
    }
  ],
  working_stories: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "userstories"
    }
  ],
  finished_stories: [
    {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "userstories"
    }
  ],
  chart: {
    values: {
      type: [mongoose.SchemaTypes.Number],
      default: []
    }
  }
});

const Sprint = mongoose.model("sprints", SprintSchema);

module.exports = Sprint;
