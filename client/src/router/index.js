import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Profile from '@/components/Profile'
import Workspace from '@/components/Workspace'
import Sprint from '@/components/workspace_routes/Sprint'
import Settings from '@/components/workspace_routes/Settings'
import HistoryComponent from '@/components/workspace_routes/History'
import Chart from '@/components/workspace_routes/Chart'
import Users from '@/components/Users'
import Fail from '@/components/Fail'
import Kostil from '@/components/Kostil'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/profile',
      name: 'Workspaces',
      component: Profile
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/fail/:code',
      name: 'Fail',
      component: Fail,
      props: true
    },
    {
      path: '/workspace/:name',
      name: 'Workspace',
      component: Workspace,
      children: [
        {
          path: 'sprint/:id',
          name: 'Sprint',
          component: Sprint,
          props: true
        },
        {
          path: 'chart/:id',
          name: 'Chart',
          component: Chart,
          props: true
        },
        {
          path: 'settings',
          name: 'Settings',
          component: Settings
        },
        {
          path: 'history',
          name: 'History',
          component: HistoryComponent
        }
      ]
    },
    {
      path: '/kostil',
      component: Kostil
    },
    {
      path: '*',
      component: Fail,
      props: {
        code: 404
      }
    }
  ]
})

export default router

router.beforeEach((to, from, next) => {
  const publicPages = ['/']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn =
    localStorage.getItem('jwt') && localStorage.getItem('jwt') !== 'null'

  if (authRequired && !loggedIn) {
    return next('/')
  }

  next()
})
