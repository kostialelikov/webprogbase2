// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import VueWebsocket from 'vue-native-websocket'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default-dark.css'
import 'vue-awesome/icons/flag'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import reactiveStorage from 'vue-reactive-storage'

Vue.config.productionTip = false
Vue.use(VueMaterial)
Vue.use(VueWebsocket, 'wss://streamline-scrum.herokuapp.com', {
  format: 'json',
  reconnection: true
})

Vue.use(reactiveStorage, {
  jwt: localStorage.getItem('jwt'),
  admin: localStorage.getItem('admin')
})

Vue.component('v-icon', Icon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  watch: {
    $route (to, from) {
      if (
        this.localStorage.jwt !== 'null' &&
        this.localStorage.jwt !== null &&
        this.localStorage.jwt !== 'undefined' &&
        this.localStorage.jwt !== undefined
      ) {
        const request = {
          method: 'add-user',
          jwt: this.localStorage.jwt
        }
        this.$socket.sendObj(request)
      } else {
        this.$socket.sendObj({ method: 'remove-user' })
      }
    }
  }
})
