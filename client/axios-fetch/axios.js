class axios {
  static async get (url, config) {
    let returned
    await fetch(url, config)
      .then(response => {
        return response.json()
      })
      .then(json => {
        returned = json
      })
    return new Promise((resolve, reject) => {
      resolve({ data: returned })
    })
  }
  static async post (url, data, config) {
    let returned
    const config_obj = Object.assign(
      {
        body: JSON.stringify(data),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      },
      config
        ? {
          headers: Object.assign(config.headers, {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          })
        }
        : {},
      {
        method: 'POST'
      }
    )
    await fetch(url, config_obj)
      .then(response => {
        return response.json()
      })
      .then(json => {
        returned = json
      })
    return new Promise((resolve, reject) => {
      resolve({ data: returned })
    })
  }
  static async put (url, data, config) {
    let returned
    const config_obj = Object.assign(
      {
        body: JSON.stringify(data),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      },
      config
        ? {
          headers: Object.assign(config.headers, {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          })
        }
        : {},
      { method: 'PUT' }
    )
    await fetch(url, config_obj)
      .then(response => {
        return response.json()
      })
      .then(json => {
        returned = json
      })
    return new Promise((resolve, reject) => {
      resolve({ data: returned })
    })
  }
  static async delete (url, config) {
    let returned
    const config_obj = Object.assign(config, { method: 'DELETE' })
    await fetch(url, config_obj)
      .then(response => {
        return response.json()
      })
      .then(json => {
        returned = json
      })
    return new Promise((resolve, reject) => {
      resolve({ data: returned })
    })
  }
}

module.exports = axios
